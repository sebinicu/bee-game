<?php

use BeeGame\Gameplay\Gameplay;

require_once realpath("vendor/autoload.php");

$gameplay = Gameplay::getInstance();
$gameplay->initialize();
$bees = $gameplay->getBees();

$queenHealth = $gameplay->getSwarmHealth('QUEEN');
$workersHealth = $gameplay->getSwarmHealth('WORKER');
$dronesHealth = $gameplay->getSwarmHealth('DRONE');


$isGameOver = $gameplay->isGameOver();

if($_SERVER['REQUEST_METHOD'] == "POST" )
{
    if(isset($_POST['hit']))
    {
        $cod = $gameplay->hit();
    }

    if(isset($_POST['reinitialize']))
    {
        $gameplay->reinitialize();
    }

    $isGameOver = $gameplay->isGameOver();
    $bees = $gameplay->getBees();
    $queenHealth = $gameplay->getSwarmHealth('QUEEN');
    $workersHealth = $gameplay->getSwarmHealth('WORKER');
    $dronesHealth = $gameplay->getSwarmHealth('DRONE');

}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .container{
            display: flex;
        }
        .worker, .drone{
            width: 450px;
        }
        .queen{
            width: 350px;
        }

        p{
            font-size: 20px;
        }
        span{
            color:red;
            padding-left: 10px;
        }

        input{
            font-size: 20px;
        }

    </style>
</head>
<body>
<h1>Player name: Eusebiu</h1>

<?php
if (!$isGameOver){
    ?>
    <form action="" method="post">
        <input type="submit" name="hit" value="HIT" />
    </form>

    <div class="container">
        <div class="col">
            <?php
            foreach ($bees as $b){
                if($b->getType() == 'QUEEN'){
                    ?>
                    <div class="queen">
                        <h2>Queen health: <?php echo $queenHealth.'hp'; if(isset($cod) && $cod == $b->getCod()) echo '<span>-' . $b->getDamage() . 'hp</span>'?></h2>
                    </div>
                    <?php

                }
            }
            ?>
        </div>
        <div class="col">
            <h2>Workers swarm health: <?php echo $workersHealth;?>hp </h2>
            <?php
            foreach ($bees as $b){
                if($b->getType() == 'WORKER' )
                    if($b->getHp() > 0){
                        ?>
                        <div class="worker">
                            <p><strong>Worker </strong><?php echo $b->getHp().'hp'; if(isset($cod) && $cod == $b->getCod()) echo '<span>-' . $b->getDamage() . 'hp</span>'?></p>
                        </div>
                        <?php
                    }else echo '<p style="color: red">Killed worker </p>';
            }
            ?>
        </div>
        <div class="col">
            <h2>Drones swarm health: <?php echo $dronesHealth;?>hp</h2>
            <?php
            foreach ($bees as $b){
                if($b->getType() == 'DRONE')
                    if($b->getHp() > 0)
                    {
                        ?>
                        <div class="drone">
                            <p><strong>Drone </strong><?php echo $b->getHp().'hp'; if(isset($cod) && $cod == $b->getCod()) echo '<span>-' . $b->getDamage() . 'hp</span>'?></p>
                        </div>
                        <?php
                    }else echo '<p style="color: red">Killed drone </p>';
            }
            ?>
        </div>
    </div>
    <?php
}else{
    ?>
    <h1>Game Over</h1>
    <h2>Queen health:<?php echo $queenHealth;?>hp </h2>
    <h2>Workers swarm health: <?php echo $workersHealth;?>hp </h2>
    <h2>Drones swarm health: <?php echo $dronesHealth;?>hp </h2>
    <form action="" method="post">
        <input type="submit" name="reinitialize" value="Reinitialize" />
    </form>
    <?php
}

?>

</body>
</html>
