<?php

declare(strict_types=1);

use BeeGame\Factory\Drone;
use PHPUnit\Framework\TestCase;

final class DroneTest extends TestCase
{
    public function testDroneType(): void
    {
        $drone = new Drone();
        $this->assertEquals(
            'DRONE',
            $drone->getType()
        );
    }

    public function testDroneHP(): void
    {
        $drone = new Drone();
        $this->assertEquals(
            50,
            $drone->getHp()
        );
    }

    public function testDroneDMG(): void
    {
        $drone = new Drone();
        $this->assertEquals(
            12,
            $drone->getDamage()
        );
    }
}
