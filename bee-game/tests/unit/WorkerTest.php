<?php

declare(strict_types=1);

use BeeGame\Factory\Worker;
use PHPUnit\Framework\TestCase;

final class WorkerTest extends TestCase
{


    public function testWorkerType(): void
    {
        $worker = new Worker();
        $this->assertEquals(
            'WORKER',
            $worker->getType()
        );
    }

    public function testWorkerHP(): void
    {
        $worker = new Worker();
        $this->assertEquals(
            75,
            $worker->getHp()
        );
    }

    public function testWorkerDMG(): void
    {
        $worker = new Worker();
        $this->assertEquals(
            10,
            $worker->getDamage()
        );
    }
}
