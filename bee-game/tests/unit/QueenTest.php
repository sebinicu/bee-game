<?php

declare(strict_types=1);

use BeeGame\Factory\Queen;
use PHPUnit\Framework\TestCase;

final class QueenTest extends TestCase
{


    public function testQueenType(): void
    {
        $queen = new Queen();
        $this->assertEquals(
            'QUEEN',
            $queen->getType()
        );
    }

    public function testQueenHP(): void
    {
        $queen = new Queen();
        $this->assertEquals(
            100,
            $queen->getHp()
        );
    }

    public function testQueenDMG(): void
    {
        $queen = new Queen();
        $this->assertEquals(
            8,
            $queen->getDamage()
        );
    }
}
