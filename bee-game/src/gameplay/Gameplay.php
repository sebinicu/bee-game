<?php

namespace BeeGame\Gameplay;
use BeeGame\Factory\BeeFactory;
use BeeGame\Repository\BeeRepository;

class Gameplay
{

    private BeeFactory $beeFactory;
    private BeeRepository $beeRepository;
    private array $bees;

    private static $instance = null;

    private function __construct()
    {
        $this->beeFactory = new BeeFactory();
        $this->beeRepository = new BeeRepository();
        $this->bees = array();
    }

    public function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new Gameplay();
        }

        return self::$instance;
    }

    public function initialize():void
    {
        array_push($this->bees, $this->beeFactory->getBee('QUEEN'));

        for($i = 0; $i<5; $i++){
            array_push($this->bees, $this->beeFactory->getBee('WORKER'));
        }

        for($i = 0; $i<8; $i++){
            array_push($this->bees, $this->beeFactory->getBee('DRONE'));
        }

        $i=0;
        foreach ($this->bees as $b){
            $b->setCod($i++);
            $this->beeRepository->save($b);
        }

        $this->bees = $this->beeRepository->getAll();
    }

    public function reinitialize():void
    {

        $this->bees = $this->beeRepository->getAll();

        foreach ($this->bees as $b){
            switch ($b->getType()){
                case 'QUEEN':{
                    $b->setHp(100);
                };
                case 'WORKER':{
                    $b->setHp(75);
                }
                case 'DRONE':{
                    $b->setHp(50);
                }
            }
            $this->beeRepository->update($b);
        }

        $this->bees = $this->beeRepository->getAll();
    }

    public function hit():int
    {
        $this->bees = $this->beeRepository->getAll();
        $rand_key = array_rand($this->bees);
        while ($this->bees[$rand_key]->getHp() == 0){
            $rand_key = array_rand($this->bees);
        }
        $hp = $this->bees[$rand_key]->getHp();
        $damage = $this->bees[$rand_key]->getDamage();
        if($hp - $damage > 0){
            $this->bees[$rand_key]->setHp($hp - $damage);
        }else
        {
            $this->bees[$rand_key]->setHp(0);
        }

        $this->beeRepository->update($this->bees[$rand_key]);

        return $this->bees[$rand_key]->getCod();
    }

    public function getBees():array
    {
        $this->bees = $this->beeRepository->getAll();
        return $this->bees;
    }

    public function getSwarmHealth($type):int
    {
        $this->bees = $this->beeRepository->getAll();
        $health = 0;
        foreach ($this->bees as $b){
            if($b->getType() == $type){
                $health += $b->getHp();
            }
        }
        return $health;
    }

    public function isGameOver():int
    {
        $queenHealth = $this->getSwarmHealth('QUEEN');
        $workersHealth = $this->getSwarmHealth('WORKER');
        $dronesHealth = $this->getSwarmHealth('DRONE');
        if($queenHealth == 0 || $workersHealth == 0 || $dronesHealth == 0){
            return 1;
        }
        return 0;
    }




}