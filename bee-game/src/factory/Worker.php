<?php

namespace BeeGame\Factory;

class Worker implements Bee
{
    private string $type;
    private int $hp;
    private int $cod;
    private int $damageTaken;

    public function __construct()
    {
        $this->type = "WORKER";
        $this->hp = 75;
        $this->damageTaken = 10;
    }

    public function getType():string
    {
        return $this->type;
    }

    public function setType($type):void
    {
        $this->type = $type;
    }

    public function getHp():int
    {
        return $this->hp;
    }

    public function setHp($hp):void
    {
        $this->hp = $hp;
    }

    public function getCod():int
    {
        return $this->cod;
    }

    public function setCod($cod):void
    {
        $this->cod = $cod;
    }

    public function getDamage():int
    {
        return $this->damageTaken;
    }

    public function setDamage($damageTaken):void
    {
        $this->cod = $damageTaken;
    }

}