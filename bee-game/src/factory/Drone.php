<?php

namespace BeeGame\Factory;

class Drone implements Bee
{
    private string $type;
    private int $hp;
    private int $cod;
    private int $damageTaken;

    public function __construct()
    {
        $this->type = "DRONE";
        $this->hp = 50;
        $this->damageTaken = 12;
    }

    public function getType():string
    {
        return $this->type;
    }

    public function setType($type):void
    {
        $this->type = $type;
    }

    public function getHp():int
    {
        return $this->hp;
    }

    public function setHp($hp):void
    {
        $this->hp = $hp;
    }

    public function getCod():int
    {
        return $this->cod;
    }

    public function setCod($cod):void
    {
        $this->cod = $cod;
    }

    public function getDamage():int
    {
        return $this->damageTaken;
    }

    public function setDamage($damageTaken):void
    {
        $this->cod = $damageTaken;
    }
}