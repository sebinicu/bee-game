<?php

namespace BeeGame\Factory;

interface Bee
{
    public function getCod():int;
    public function setCod($cod):void;
    public function getType():string;
    public function setType($type):void;
    public function getHp():int;
    public function setHp($hp):void;
    public function getDamage():int;
    public function setDamage($damageTaken):void;
}