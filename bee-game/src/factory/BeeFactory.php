<?php

namespace BeeGame\Factory;

class BeeFactory
{

    public function getBee($beeType):Bee
    {
        switch ($beeType) {
            case 'QUEEN':
                return new Queen();
            case 'WORKER':
                return new Worker();
            case 'DRONE':
                return new Drone();
            default:
                null;
        }
    }
}
