<?php

namespace BeeGame\Repository;

use BeeGame\Factory\Bee;

interface IRepository
{
    public function get($cod):Bee;
    public function getAll();
    public function save($bee);
    public function delete($cod);
    public function update($bee);
}