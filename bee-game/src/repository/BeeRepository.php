<?php

namespace BeeGame\Repository;

use BeeGame\Factory\Bee;

class BeeRepository implements IRepository {

    public function get($cod): Bee
    {
        $bees = $this->citireFisier();
        foreach ($bees as $b){
            if($b->getCod() == $cod){
                $bee = $b;
            }
        }
        if($bee->getCod() != $cod){
            return null;
        }else{
            return $bee;
        }
    }

    public function getAll():array
    {
        $bees = $this->citireFisier();
        return $bees;
    }

    public function save($bee):void
    {
        $bees = $this->citireFisier();
        if(!$bees)
            $bees = array();
        $exists = 0;
        foreach ($bees as $key=>$b){
            if($b->getCod() == $bee->getCod()){
                $exists = 1;
            }
        }

        if(!$exists){
            array_push($bees, $bee);
            $this->scriereFisier($bees);
        }

    }

    public function delete($cod):void
    {
        $bees = $this->citireFisier();
        foreach ($bees as $b){
            if($b->getCod() == $cod){
                unset($bees[$cod]);
            }
        }
        $this->scriereFisier($bees);
    }

    public function update($bee):void
    {
        $bees = $this->citireFisier();
        foreach ($bees as $key=>$b){
            if($b->getCod() == $bee->getCod()){
                $bees[$key] = $bee;
            }
        }
        $this->scriereFisier($bees);
    }

    public function citireFisier()
    {
        if(file_exists('bees.txt')){
            $beesFromFile = file_get_contents('bees.txt');
            return unserialize($beesFromFile);
        }
        return null;
    }

    public function scriereFisier($beesFromFile)
    {
        $serializeBees = serialize($beesFromFile);
        file_put_contents('bees.txt', $serializeBees);
    }
}